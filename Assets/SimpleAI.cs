﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleAI : MonoBehaviour {

	private GameObject target;
	private float smoothTime = 3F;
	private Vector3 velocity = Vector3.zero;
	public bool IsFighting = false;
	private float distance;

	List <GameObject> enemies = new List<GameObject>();

	// Use this for initialization
	void Start () {
		if (this.tag == "Friend" && IsFighting==false) {
			target = GameObject.FindGameObjectWithTag ("Enemy");
			if (target.GetComponent<SimpleAI> ().IsFighting == false)
				foreach (GameObject g in enemies) {
				}
			
		} else if (this.tag=="Enemy" && IsFighting==false){
			target = GameObject.FindGameObjectWithTag ("Friend");
			//target = GameObject.FindGameObjectWithTag ("Friend");

		}
		
	}
	
	// Update is called once per frame
	void Update () {



//		if (target.GetComponent<SimpleAI>().IsFighting==true)
//			target=GameObject.Find
		//Vector3 targetPosition = target.TransformPoint(new Vector3(0, 5, -10));
		distance = Vector3.Distance(this.transform.position,target.transform.position);

		if (distance > 2) {
			Debug.Log ("Distance more 2");
			transform.position = Vector3.SmoothDamp (transform.position, target.transform.position, ref velocity, smoothTime);
		}else
			IsFighting = true;

		}
		
	void EnemyController(){
		
		if (target.GetComponent<SimpleAI>().IsFighting==true)
			target = GameObject.FindGameObjectWithTag ("Enemy");
	}

	void FriendController(){
		
		if (target.GetComponent<SimpleAI>().IsFighting==true)
			target = GameObject.FindGameObjectWithTag ("Friend");
	}

	}

