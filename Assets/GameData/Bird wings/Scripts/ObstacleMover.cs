﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Moves obstacles along z-axis and destroies, when not seen.
/// </summary>
public class ObstacleMover : MonoBehaviour {
	
	private float Speed;
	public float minDistance;
	Transform deadZonePoint;

	void Start () {
		deadZonePoint = GameObject.Find ("DeadZone").transform;
	}

	void Update () {
        Speed = FindObjectOfType<SpawnController>().speed;
		transform.Translate (0, 0, Speed * Time.deltaTime);
		if(Vector3.Distance(transform.position, deadZonePoint.position) < minDistance) {
			Destroy (gameObject);
		}
	}
}
