﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.PostProcessing;

/// <summary>
/// Controlls position of the player based on player's pose.
/// </summary>
public class PlayerContoller : MonoBehaviour {

	public DistanceChecker leftChecker;
	public DistanceChecker rightChecker;
	public DistanceChecker JumpChecker;
	public DistanceChecker CrouchChecker;

	public ParticleSystem FeatherEffect;
	public ParticleSystem StoneHitEffect;

	public AudioClip SoundofDump;
	public AudioSource Source;

	[Header ("Skelet data")]
	public Transform Hip;
	public Transform Neck;

	[Header ("Positions")]
	public Transform RightPosition;
	public Transform LeftPosition;
	public Transform MiddlePosition;
	public Transform UpPosition;
	public Transform DownPosition;
	public Transform mainCamera;

	public Vector3 InitialHipPosition;

	public PostProcessingBehaviour ppb;

	Vector3 targetPos;
	Vector3 vel;
	float cameraVel;
	float PainEffectVel;
	float TimeScaleVel;

	float Tilt = 6;
	float TiltTarget = 6;

	public bool m_Jump;

	[Header ("Rules for distances")]
	public float angle;
	public float rightAngle;
	public float leftAngle;

//	public float targeySquat;
//	public float CrouchDistance = 1;

	[Header ("Ash's Variables")]
	#region Ash's Variables
	public float AmountOfShaking = 0.5f;
	public float Decay = 0.007f;
	public float shake_intensity;
	public float shake_decay;
	public Vector3 originPosition;

	#endregion

	LevelController m_LevelController;

	void Start () {
		m_LevelController = FindObjectOfType<LevelController> ();
		Destroy (RightPosition.GetComponent<MeshRenderer> ());
		Destroy (LeftPosition.GetComponent<MeshRenderer> ());
		Destroy (MiddlePosition.GetComponent<MeshRenderer> ());
		Destroy (UpPosition.GetComponent<MeshRenderer> ());
		Destroy (DownPosition.GetComponent<MeshRenderer> ());
	}

	//TODO clean up this
	// Update is called once per frame
	void Update () {
		CheckObstacleBelow ();
		transform.position = Vector3.SmoothDamp (transform.position, targetPos, ref vel, 0.2f);
		Stabalize ();
		if (!m_LevelController.IsPaused) {
			Time.timeScale = Mathf.Clamp01 (Mathf.SmoothDamp (Time.timeScale, 1, ref TimeScaleVel, 0.1f));
		} else {
			TiltTarget = 6;
		}
		Tilt = Mathf.SmoothDamp (Tilt, TiltTarget, ref cameraVel, 0.2f);
		Source.pitch = Time.timeScale;
		mainCamera.localEulerAngles = new Vector3 (mainCamera.localEulerAngles.x, mainCamera.localEulerAngles.y, Tilt);
		//Distance calculator
		FindObjectOfType<ScoreManager> ().UpdateScore (Time.deltaTime * FindObjectOfType<SpawnController> ().speed, false);
	}

	public void TurnLeft() {
		m_Jump = false;
		TiltTarget = 0;
		targetPos = LeftPosition.position;
	}

	public void TurnRight() {
		m_Jump = false;
		TiltTarget = 12;
		targetPos = RightPosition.position;
	}

	public void Crouch() {
		TiltTarget = 6;
		targetPos = DownPosition.position;
		m_Jump = false;
	}

	public void Jump() {
		TiltTarget = 6;
		targetPos = UpPosition.position;
		m_Jump = true;
	}

	public void Stable() {
		if (!m_Jump) {
			TiltTarget = 6;
			targetPos = MiddlePosition.position;
		}
	}

	public void Enable () {
		GameObject.Find ("BackMusic").GetComponent<AudioSource> ().UnPause ();
	}

	public void Disable () {
		GameObject.Find ("BackMusic").GetComponent<AudioSource> ().Pause ();
	}

	/// <summary>
	/// Call <see cref="Hit()"/> method when hit an obstacle.
	/// </summary>
	/// <param name="obj">Obstacle that was hit.</param>
	void OnTriggerEnter (Collider obj) {
		m_Jump = false;
		Hit ();
	}

	/// <summary>
	/// Call this method to hurt player.
	/// </summary>
	public void Hit () {
		PostProcessingProfile profile = ppb.profile;

		StoneHitEffect.transform.position = new Vector3 (StoneHitEffect.transform.position.x, StoneHitEffect.transform.position.y, StoneHitEffect.transform.position.z);
		StoneHitEffect.Play ();
		profile.vignette.enabled = true;
		VignetteModel.Settings g = profile.vignette.settings;
		g.intensity = 0.5f;
		profile.vignette.settings = g;
		Time.timeScale = 0.1f;
		FeatherEffect.transform.position = new Vector3 (FeatherEffect.transform.position.x, FeatherEffect.transform.position.y, FeatherEffect.transform.position.z);
		FeatherEffect.Play ();
		Source.PlayOneShot (SoundofDump);
		Source.pitch = 0.25f;
		FindObjectOfType<LevelController> ().GameOver ("GAMEOVER", "You didn't survive...");
	}

	/// <summary>
	/// Stabalize player's view.
	/// </summary>
	public void Stabalize () {
		PostProcessingProfile profile = ppb.profile;

		profile.vignette.enabled = true;
		VignetteModel.Settings g = profile.vignette.settings;
		g.intensity = Mathf.SmoothDamp (g.intensity, 0.25f, ref PainEffectVel, 0.75f);
		profile.vignette.settings = g;
	}

	/// <summary>
	/// Shake player's camera for creating flying effect.
	/// </summary>
	IEnumerator Shake () {
		originPosition = transform.localPosition;
		shake_intensity = AmountOfShaking;
		shake_decay = Decay;
		while (shake_intensity > 0) {
			transform.localPosition = originPosition + UnityEngine.Random.insideUnitSphere * shake_intensity;
			yield return new WaitForSeconds (0.01f);
			shake_intensity -= Decay;
		}
	}

	/// <summary>
	/// It makes character to jump, when the user jumps.
	/// </summary>
	public void CheckObstacleBelow () {
		Vector3 downfwd = transform.TransformDirection (Vector3.down);
		RaycastHit hit;
		if (Physics.Raycast (transform.position, downfwd, out hit)) {
			if (hit.collider.CompareTag ("EndJumpObstacle")) {
				m_Jump = false;
				Stable ();
			}
		}
	}

	/// <summary>
	/// It makes character to crouch, when the user squat.
	/// </summary>
	//	public void Crouching () {
	//		Vector3 upfwd = transform.TransformDirection(Vector3.up);
	//		if (Physics.Raycast (transform.position, upfwd, 5)) {
	//			Jump = false;
	//			Crouch = true;
	//		} else {
	//			Crouch = false;
	//		}
	//	}
}
