﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Distance checker is used to create poses of a player.
/// </summary>
public class DistanceChecker : MonoBehaviour {
	public Transform trackPoint;

	public float maxDistance = 0.7f;
	public float minDistance = 0.7f;
	public float Distance;
	public float DistanceFactor;
	public DistanceChecker m_DistanceChecker;
	public bool ReturnTrueWhenLess = true;//Check this to get true result when distance is less, or uncheck if want to get true when more
	public bool DebugDistance;

	public float Timer = 1;

	void Update() {
		Distance = Vector3.Distance (transform.position, trackPoint.position);
		bool right;
		if (ReturnTrueWhenLess) {
			right = Distance < minDistance;
		} else {
			right = Distance > maxDistance;
		}
		if (m_DistanceChecker && Input.GetKeyDown (KeyCode.U)) {
			UpdateMaxDistance ();
		}
		if (right) {
			Debug.DrawLine (transform.position, trackPoint.position, Color.green);
			if (DebugDistance) {
				Debug.Break ();
			}
		} else {
			Debug.DrawLine (transform.position, trackPoint.position, Color.red);
		}
		if (Timer > 0) {
			Timer -= Time.deltaTime;
			if (Timer < 0) {
				Timer = 0;
				CalcHeight ();
			}
		}
	}

	/// <summary>
	/// It detects status of pose
	/// </summary>
	/// <returns><c>true</c>, if trackPoint is in a right place, <c>false</c> otherwise.</returns>
	public bool GetResult() {
		if (ReturnTrueWhenLess) {
			return Distance < minDistance;
		} else {
			return Distance > maxDistance;
		}
	}

	public void UpdateMaxDistance() {
		Debug.Log ("Calculating...");
		Timer = 1;
	}

	public void CalcHeight() {
		if (DistanceFactor != 0) {
			if (ReturnTrueWhenLess) {
				minDistance = m_DistanceChecker.Distance*DistanceFactor;
			} else {
				maxDistance = m_DistanceChecker.Distance*DistanceFactor;
			}
			Debug.Log ("Finished calc");
		}
	}
}
