﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Spawns random obstacles at specific speed and changes it for all of them.
/// </summary>
public class SpawnController : MonoBehaviour {

	public float speed;
	public float minDistance;
	public float TimerForMaxSpeed = 110;
	public float MaxSpeed = 30;
	Transform lastObject;
	public GameObject[] Prefabs;

	float vel = 0.0f;

	void Start () {
		Prefabs = Resources.LoadAll<GameObject> ("Prefab/" + SceneManager.GetActiveScene ().name);
	}

	void Update () {
		if (Input.GetKeyDown ("space")) {
			StartCoroutine (UpdateSpeed (100, 1));
		}
		speed = Mathf.SmoothDamp (speed, MaxSpeed, ref vel, TimerForMaxSpeed);
		if (!lastObject || Vector3.Distance (transform.position, lastObject.position) > minDistance) {
			SpawnObstacle ();
		}
	}

	/// <summary>
	/// Spawns the obstacle.
	/// </summary>
	void SpawnObstacle () {
		Debug.Log ("Spawning");
		lastObject = Instantiate (Prefabs [Random.Range (0, Prefabs.Length)], transform.position, Quaternion.identity).transform;
	}

	/// <summary>
	/// Updates the speed of all obstacles.
	/// </summary>
	/// <param name="newSpeed">New temporary speed.</param>
	/// <param name="time">Duration of new speed.</param>
	IEnumerator UpdateSpeed (float newSpeed, float time) {
		float oldSpeed = speed;

		ObstacleMover[] objects = FindObjectsOfType<ObstacleMover> ();
		foreach (ObstacleMover t in objects) {
			speed = newSpeed;
		}

		yield return new WaitForSeconds (time);

		objects = FindObjectsOfType<ObstacleMover> ();
		foreach (ObstacleMover t in objects) {
			speed = oldSpeed;
		}
	}
}
