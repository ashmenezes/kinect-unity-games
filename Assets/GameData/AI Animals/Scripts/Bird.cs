﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// AI for bird. It lands on a person if a person let to do it,
/// </summary>
public class Bird : MonoBehaviour {

	public float CriticalAngle;
	public float Angle;
	public Transform Target;
	public Transform TrackedParent;
	public float JumpHieght =1;
	public float Timer;
	public float Delay = 0.3f;

	bool Landed;
	Animator AC;
	ParticleSystem Feathers;
	float TimeOfReaching;
	Vector3 DefaultPoint;
	Vector3 vel;
	Vector3 localVel;
	AudioSource AS;

	void Start() {
		Feathers = GameObject.Find ("Feathers").GetComponent <ParticleSystem> ();
		AS = GetComponent <AudioSource> ();
		AC = GetComponentInChildren <Animator> ();
		TimeOfReaching = Random.Range (0.2f, 0.4f);
		Delay = Random.Range (0f, 1f);
		Timer = Delay;
		DefaultPoint = transform.position;
	}

	void Update() {
		Vector3 targetPos = Target.position - TrackedParent.position;
		Angle = Vector3.Angle(targetPos, Vector3.down);
		if (Angle < CriticalAngle) {
			transform.parent = null;
			AC.SetBool ("Flying",true);
			transform.position = Vector3.SmoothDamp (transform.position, DefaultPoint, ref vel, TimeOfReaching,7);
			if (Landed) {
				Landed = false;
				Feathers.transform.position = transform.position;
				Feathers.Play ();
			}
		} else {
			if (Vector3.Distance (transform.position, Target.position) < 0.4f) {
				transform.localEulerAngles = new Vector3 (0, 0, 0);
				Landed = true;
				AC.SetBool ("Flying",false);
				transform.parent = Target;
				transform.localPosition = Vector3.SmoothDamp (transform.localPosition, Target.localPosition, ref localVel, TimeOfReaching);
				if (Timer > 0) {
					Timer -= Time.deltaTime;
					if (Timer < 0) {
						Delay = Random.Range (1f, 4f);
						Timer = Delay;
						Jump ();
					}
				}
			} else {
				transform.parent = null;
				AC.SetBool ("Flying",true);
				transform.position = Vector3.SmoothDamp (transform.position, Target.position, ref vel, TimeOfReaching);
			} 
		}
	}

	/// <summary>
	/// Changes land point
	/// </summary>
	/// <param name="target">Land point.</param>
	public void SetTarget(Transform target) {
		Target = target;
		if (!TrackedParent) {
			TrackedParent = target.parent;
		}
	}

	/// <summary>
	/// Simulate bird jump
	/// </summary>
	public void Jump() {
		AS.Play ();
		transform.Translate (0,JumpHieght,0,Space.Self);
	}
}
