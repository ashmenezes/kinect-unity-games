﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// <seealso cref="TigerHelper"/> uses hand follower to track tiger's head with user's hand.
/// </summary>
public class THHandFollower : MonoBehaviour {
	
	public Transform target;
	public float maxAngle = 35.0f;
	private Quaternion baseRotation;
	private Quaternion targetRotation;

	void Start () {
		baseRotation = transform.rotation;
	}

	void Update () {
		transform.LookAt (target);
		transform.localEulerAngles = new Vector3 (transform.localEulerAngles.x, transform.localEulerAngles.y, Mathf.Clamp (transform.localEulerAngles.z, 230, 320));
	}
}
