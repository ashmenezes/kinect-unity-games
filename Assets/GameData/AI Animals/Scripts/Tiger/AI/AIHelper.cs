﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// It will help AI to choose tasks and complete quests, using <see cref="AIActivity"/>.
/// </summary>
public class AIHelper : MonoBehaviour {

	public List<AIActivity> Activities = new List<AIActivity>();

	//Here has to be method, which returns random activity, based on type of request.
	//Type Of request can be OldActivity.

	void Start() {
		StartCoroutine (ValidateActivities ());
	}

	public AIActivity GetNextActivity(AIActivity OldActivity) {
		if (OldActivity.TypeOfActivity == AIActivity.TypesOfActivities.None || OldActivity.TypeOfActivity == AIActivity.TypesOfActivities.Reload) {
			foreach (var item in Activities) {
				if (item.TypeOfActivity == AIActivity.TypesOfActivities.Task) {
					return item;
				}
			}
		} else {
			foreach (var item in Activities) {
				if (item.TypeOfActivity == AIActivity.TypesOfActivities.Reload) {
					return item;
				}
			}
		}
		return new AIActivity ();
	}

	public AIActivity GetRandomNextActivity() {
		return Activities [Random.Range (0, Activities.Count)];
	}

	public IEnumerator ValidateActivities() {
		yield return new WaitForEndOfFrame ();
		for (int i = 0; i < Activities.Count; i++) {
			//Debug.Log ("Reading " + Activities [i].Name);
			if (Activities [i].TypeOfActivity == AIActivity.TypesOfActivities.Task) {
				//Debug.Log (Activities [i].Name + " is a Task");
				if (!Activities [i].m_GO.GetComponent <Quest> ()) {
					//Debug.Log ("Removing " + Activities [i].Name);
					Activities.Remove (Activities [i]);
				} else {
					//Debug.Log (Activities [i].Name + " has Quest");
				}
			}
		}
	}
}
