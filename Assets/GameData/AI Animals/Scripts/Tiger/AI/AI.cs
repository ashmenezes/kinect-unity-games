﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AI : MonoBehaviour {

	//We need to create Modes or States of the AI
	//It can be enum or just numbers or strings
	public enum States {
		Idle, Patrolling, Chasing
	}

	public States State;
	private NavMeshAgent Agent;
	private Animator AC; //Animator Controller
	public Transform Destination;
	//public GameObject [] PatrolPoints;
	public int IndexPoint = 0;
	//private PlayerController Player;
	//public Transform Eyes;

	//We may need task time... but we will decide that later on

	[Header("For Timing")]
	public float Timer = 2;
	[Tooltip("Time for Patrol point")]
	public float TFPP = 5;
	[Tooltip("Time to search")]
	public float TTS = 10;
	[Tooltip("Reaction time")]
	/// <summary>
	/// Reaction limit
	/// </summary>
	public float RL = 2;
	/// <summary>
	/// Reaction timer
	/// </summary>
	public float RT = 0;

	/// <summary>
	/// The attempts counter for <see cref="AIActivity"/>.
	/// </summary>
	int AttemptsCounter;
	
	[Header("For Distance")]
	public float Distance;

	AIHelper m_AIHelper;//Just assign using GetComponent
	public AIActivity m_AIActivity;

	[Header("For Debug Mode")]//<- it is better to keep variables in groups using Header attribute like this
	[Tooltip("This is for styling GUI")]//<- for hovering
	public GUISkin SkinForGUI;

	// Use this for initialization
	void Start () {
		m_AIHelper = GetComponent <AIHelper> ();
		Agent = GetComponent <NavMeshAgent> ();
		AC = GetComponent <Animator> ();
		//Player = FindObjectOfType<PlayerController> ();
		//PatrolPoints = GameObject.FindGameObjectsWithTag ("Patrol Point");
//		if (State == States.Patrolling) {
//			goToPatrolling ();
//		}
		UpdateActivity();
	}

	// Update is called once per frame
	void Update () {
		//AC.SetFloat ("AI Velocity", Agent.velocity.magnitude);
		if (m_AIActivity.m_GO) {
			Distance = Vector3.Distance (transform.position, m_AIActivity.m_GO.transform.position);
		}
		if (State == States.Idle) {
			DecisionMaking ();
		}
		#region Deprected
		//Rules for Distances needed
		/*if (Player && !Physics.Linecast (Eyes.position, Player.transform.position)) {
			if (RT <= RL) {//This condition can be also improved for balancing game: limit for timer can be even less, not exactly equal to TTS
				RT += Time.deltaTime;//It should multiplied by something for balance, but it could be?
				if (RT > RL || State == States.Chasing) {
					AC.SetBool ("Search", false);
					RT = RL;
					goToChasing ();
				}
			}
		} else {
			DecisionMaking ();
			if (RT > 0) {
				RT -= Time.deltaTime;
				if (RT < 0) {
					RT = 0;
				}
			}
		}
		if (Player) {
			Debug.DrawLine (Eyes.position, Player.transform.position, Color.red);
		}*/
		#endregion
	}

	private void DecisionMaking () {
		if (Timer > 0) {
			if (Agent.remainingDistance < 1.5f ) {
				if (Timer == m_AIActivity.DelayBetweenAttempts) {
					AC.SetBool (m_AIActivity.NameOfAnimState,true);
				}
				Timer -= Time.deltaTime;
			}
			//AC.SetBool ("Search", Agent.velocity.magnitude < 1f);
			if (Timer <= 0) {
				Timer = 0;
				if (AttemptsCounter > 0) {
					AttemptsCounter--;
					if (m_AIActivity.TypeOfActivity == AIActivity.TypesOfActivities.Task) {
						if (m_AIActivity.m_GO.GetComponent <Quest> ().TriggerEvent ()) {
							Debug.Log ("I have succeeded to complete " + m_AIActivity.Name);
							//m_AIHelper.Activities.Remove (m_AIActivity);
							UpdateActivity ();
						} else {
							Debug.Log ("I failed to complete " + m_AIActivity.Name + ", attempting " + AttemptsCounter + " times.");
						}
					}
				} else {
					AC.SetBool (m_AIActivity.NameOfAnimState,false);
					UpdateActivity ();
				}
				Timer = m_AIActivity.DelayBetweenAttempts;
			}
		}
	}

	void UpdateActivity() {
        Debug.Log ("No more attempts for " + m_AIActivity.Name + " are left.");

        #region Ash's arm modifications
        // Pat is now an activity,
        if (FindObjectOfType<TigerHelper>().down) // If the arm is down
        {
            m_AIActivity = m_AIHelper.Activities[2]; // Set the next activity to Pat
        }
        else {
        #endregion
            m_AIActivity = m_AIHelper.GetRandomNextActivity(); // Else get a random activity
        }
        // This has the side-effect that makes the Tiger randomly go to Pat,
		Debug.Log ("Going to " + m_AIActivity.Name);
		AttemptsCounter = m_AIActivity.AmountOfAttempts;
		if (m_AIActivity.TypeOfActivity != AIActivity.TypesOfActivities.None) {
			UpdateDestination (m_AIActivity.m_GO.transform.position);
		}
	}

	/// <summary>
	/// Goes to idle.
	/// </summary>
	#region Switching Tasks
	public void goToIdle () {
		State = States.Idle;
		Agent.Stop ();
	}
	/// <summary>
	/// Goes to patrolling.
	/// </summary>
//	public void goToPatrolling () {
//		Agent.Resume ();
//		AC.SetBool ("Search", false);
//		State = States.Patrolling;
//		if (Agent.remainingDistance <= 1.5f) {
//			IndexPoint++;
//		}
//		if (IndexPoint > PatrolPoints.Length - 1) {
//			IndexPoint = 0;
//		}
//		Timer = TFPP;
//		UpdateDestination (PatrolPoints[IndexPoint].transform.position);
//	}
	/// <summary>
	/// Goes to chasing.
	/// </summary>
//	public void goToChasing () {
//		State = States.Chasing;
//		Timer = TTS;
//		Agent.Resume ();
//		UpdateDestination (Player.transform.position);
//	}
	#endregion



	//Updates the destination point/recalculates
	public void UpdateDestination (Vector3 pos) {
        Agent.SetDestination(pos);
		//Destination.position = pos;
	}
	/// <summary>
	/// Raises the GU event.
	/// </summary>
	//Pop up with Q
	void OnGUI () {
		GUI.skin = SkinForGUI;
		if (Input.GetKey(KeyCode.Q)) {
			GUILayout.BeginHorizontal ();

			GUILayout.Space (Screen.width*0.1f);//Since it is space in horizontal group it is also left padding
			GUILayout.BeginVertical ();
			GUILayout.Space (Screen.height*0.1f);//Since it is space in Vertical group it is also top padding
			if (GUILayout.Button 	("Go to Idle Mode")) {//We need to replace it on calling some specific methods, which changes behaviour of the AI
				goToIdle ();
			}
			GUILayout.EndVertical ();

			GUILayout.Space (Screen.width*0.1f);
			GUILayout.BeginVertical ();
			GUILayout.Space (Screen.height*0.1f);//Since it is space in Vertical group it is also top padding
			GUILayout.Box ("AI Stats:" +
				"\nTask Timer " + Mathf.RoundToInt (Timer*100f)/100f + " sec" +
				"\nReaction Timer " + Mathf.RoundToInt (RT/RL*100f) + " %" +
				"" );
			GUILayout.EndVertical ();
			GUILayout.EndHorizontal ();
		}
	}
}