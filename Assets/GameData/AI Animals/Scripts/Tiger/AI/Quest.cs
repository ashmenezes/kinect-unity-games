﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Quest class is used to let a player to progress in the game.
/// </summary>
public class Quest : MonoBehaviour {

	public bool isParentQuest = false;

	public string QuestName = "Quest Name";
	public string QuestDesc  = "Lorum ipsum";

	/// <summary>
	/// Every Sub Quest has to have a Parent Quest. If it does not, the quest will be considered as a Parent Quest.
	/// </summary>
	public Quest ParentQuest;
	public List<Quest> SubQuests = new List<Quest>();

	[HideInInspector]
	public Vector3 PointForQuestWindow;
	[HideInInspector]
	public Quaternion RotationForQuestWindow;
	[HideInInspector]
	public Vector3 SizeForQuestWindow;

	public GUISkin SkinForGUI;
	private bool DebugMode;
	/// <summary>
	/// Sound of Parent Quest.
	/// </summary>
	AudioClip PQSFX;
	/// <summary>
	/// Sound of Sub Quest.
	/// </summary>
	AudioClip SQSFX;

	void Awake () {
		SkinForGUI = Resources.Load <GUISkin> ("Debug");
		PQSFX = Resources.Load <AudioClip> ("Sounds/SFX/QuestSFX/PQ");
		SQSFX = Resources.Load <AudioClip> ("Sounds/SFX/QuestSFX/SQ");
		gameObject.AddComponent<AudioSource> ();
		if (!ParentQuest && gameObject.GetComponentInParent <Quest> ()) {
			ParentQuest = gameObject.GetComponentInParent <Quest> ();
			if (ParentQuest.name == this.name) {
				ParentQuest = null;
			}
		}
		if (PlayerPrefs.HasKey (QuestName)) {//So that the player doesn't repeat a completed quest again.
			if (ParentQuest) {
				ParentQuest.SubQuests.Remove (this);
			}
			Destroy (this);
		}
	}

	public bool TriggerEvent () {
		if (!isParentQuest) {
			if (ParentQuest) {
				ParentQuest.SubQuests.Remove (this);
			}
			CompleteQuest ();
			return true;
		} else if (SubQuests.Count == 0) {
			CompleteQuest ();
			return true;
		}
		return false;

	}

	/// <summary>
	/// Removes quests elements from the list if they are empty.
	/// </summary>
	public void RemoveSubQuest (Quest q) {
		foreach (var item in SubQuests) {
			if (item == q) {
				SubQuests.Remove (item);
			}
		}
	}

	void CompleteQuest () {
		Debug.Log (QuestName + " is completed!");
		if (!ParentQuest) {
			GetComponent <AudioSource> ().PlayOneShot (PQSFX);
		} else {
			GetComponent <AudioSource> ().PlayOneShot (SQSFX);
		}
		PlayerPrefs.SetInt (QuestName, 1);//saves primitives (strings, ints and floats), 1 to indicate that it is completed.
		Destroy (this, 0.5f);
	}

//	public void DisplayQuestDetails () {
//		GameObject QuestWindow = GameObject.Find ("QuestWindow");
//		QuestWindow.GetComponent <QuestWindow>().UpdatePos (PointForQuestWindow, transform.position,SizeForQuestWindow,RotationForQuestWindow);
//		Text[] tempTexts = QuestWindow.GetComponentsInChildren<Text> ();
//		tempTexts [0].text = QuestName;
//		tempTexts [1].text = QuestDesc;
//	}

	void OnGUI () {
		GUI.skin = SkinForGUI;
		if (DebugMode) {
			GUILayout.BeginHorizontal ();

			GUILayout.Space (Screen.width*0.1f);
			GUILayout.BeginVertical ();
			GUILayout.Space (Screen.height*0.1f);
			GUILayout.Box ("Quest Name: " + QuestName);
			if (GUILayout.Button ("Trigger the Event")) {
				TriggerEvent ();
			}
			if (GUILayout.Button ("Complete the quest")) {
				CompleteQuest ();
			}
			GUILayout.EndVertical ();
			GUILayout.EndHorizontal ();
		}
	}

	public void SwitchDebugMode () {
		DebugMode = !DebugMode;
	}

}