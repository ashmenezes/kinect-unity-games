﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Here AI will know how much time to spend on a <see cref="Quest"/> and where to go
/// </summary>
[System.Serializable]
public class AIActivity {

	public enum TypesOfActivities
	{
		None,
		Task,//Quest 
		Reload//Point to reload attempts
	}

	public string Name;
	public string NameOfAnimState = "Idle";
	public TypesOfActivities TypeOfActivity = TypesOfActivities.None;
	public GameObject m_GO;
	public int AmountOfAttempts;
	public float DelayBetweenAttempts = 2;

	public AIActivity() {

	}
}
