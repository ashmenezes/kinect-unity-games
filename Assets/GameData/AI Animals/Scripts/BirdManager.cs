﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Bird manager initialises AI <seealso cref="Bird"/>.
/// </summary>
public class BirdManager : MonoBehaviour {

	public Bird m_Bird;
	public Transform[] SpawnPoints;

	/// <summary>
	/// Spawns birds with random delay.
	/// </summary>
	public IEnumerator SpawnBirds() {
		if (FindObjectsOfType <Bird> ().Length == 0) {
			for (int i = 0; i < SpawnPoints.Length; i++) {
				if (SpawnPoints [i].childCount == 0) {
					Instantiate (m_Bird, transform.position, Quaternion.identity).SetTarget (SpawnPoints [i]);
					yield return new WaitForSeconds (Random.Range (0.1f, 0.6f));
				}
			}
		} else {
			FindHuman ();
		}
	}

	/// <summary>
	/// Massivelly update land point for all birds.
	/// </summary>
	/// <param name="m_transform">Land point.</param>
	public void SetTargets(Transform m_transform = null) {
		if (m_transform == null) {
			m_transform = transform;
		}
		for (int i = 0; i < SpawnPoints.Length; i++) {
			if (SpawnPoints [i].childCount >0) {
				SpawnPoints [i].GetComponentInChildren <Bird> ().SetTarget (m_transform);
			}
		}
	}

	/// <summary>
	/// Set human joints as land points for all birds.
	/// </summary>
	public void FindHuman() {
		Bird[] maBird = FindObjectsOfType <Bird> ();
		for (int i = 0; i < maBird.Length; i++) {
			maBird [i].SetTarget (SpawnPoints[i]);
		}
	}

	#if UNITY_EDITOR
	void OnGUI() {
		if (Input.GetKey (KeyCode.Q)) {
			GUILayout.BeginHorizontal ();

			GUILayout.BeginVertical ();

			if (GUILayout.Button ("SpawnBirds()")) {
				StartCoroutine (SpawnBirds ());
			}
			if (GUILayout.Button ("SetTargets(transform)")) {
				SetTargets ();
			}
			if (GUILayout.Button ("FindHuman()")) {
				FindHuman ();
			}

			GUILayout.EndVertical ();

			GUILayout.EndHorizontal ();
		}
	}
	#endif

}
