﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TigerHelper : MonoBehaviour {

	public Transform Hand;
	public AI Tiger;
	Vector3 vel;
	public bool down;

	void Update () {
		if (Vector3.Distance (transform.position, Hand.position) < 1) {
			down = true;
			Tiger.UpdateDestination (transform.position);
		} else {
			down = false;
		}
	}

	#if UNITY_EDITOR
	void OnGUI () {
		GUILayout.Box ("Distance " + Vector3.Distance (transform.position, Hand.position));
	}
	#endif
}
