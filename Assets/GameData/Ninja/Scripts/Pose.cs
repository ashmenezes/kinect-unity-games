﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Pose {

	public string Name;
	public DistanceChecker[] PosePoint;

	public bool isPoseCorrect () {
		foreach (var item in PosePoint) {
			if (!item.GetResult ()) {
				return false;
			}
		}
		return true;
	}

	public void CalibratePoints() {
		foreach (var item in PosePoint) {
			item.transform.position = item.trackPoint.position;
			item.SendMessage ("Froze",item.trackPoint,SendMessageOptions.DontRequireReceiver);
		}
	}
}
