﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// It shots stuff into Player. Call Fire to this..
/// </summary>
public class PropsThrower : MonoBehaviour {
	
	public Rigidbody Projectile;
	public GameObject LookAtTarget;
	public AudioClip ShotSound;
	public float YForce = 2;
	public float ZForce = 10;

	#region Deprected vars
	private int NumberOfProjectile;
	private bool shoot = true;
	#endregion

	void Start (){
		LookAtTarget = GameObject.Find ("RightFoot");
	}

	void Update () {
		NumberOfProjectile = GameObject.FindGameObjectsWithTag ("Projectile").Length;
		transform.LookAt(LookAtTarget.transform.position);
		if (Input.GetKeyDown (KeyCode.Space)) {
			Fire ();
		}
	}

	/// <summary>
	/// Fires projectile.
	/// </summary>
	void Fire(){
		Rigidbody go = Instantiate (Projectile, transform.position, Quaternion.identity);
		go.AddForce (transform.up*YForce + transform.forward*ZForce, ForceMode.Impulse);
		GameObject tempAS = GameObject.Find ("GlobalShortAS");
		tempAS.transform.position = transform.position;
		tempAS.GetComponent <AudioSource> ().PlayOneShot (ShotSound);
	}

	void OnGUI() {
		if (Input.GetKey (KeyCode.Q)) {
			if (GUILayout.Button ("Fire()")) {
				Fire ();
			}
		}
	}
}	


