﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour {

	public KnightController Player;
	public TextMesh DebugDistance;
	public AudioClip[] FootSteps;

	public float Timer = 0.2f;
	ScoreObject SO;
	AudioSource AS;
	Animator AC;
	bool isAttacking;
	bool isAlive = true;
	NavMeshAgent agent;

	void Start () {
		SO = GetComponent <ScoreObject> ();
		AS = GetComponent <AudioSource> ();
		AC = GetComponent <Animator> ();

		agent = GetComponent <NavMeshAgent> ();
	}

	void Update () {
		#if UNITY_EDITOR
		if (Player) {
			Color c = agent.remainingDistance <= 2 ? Color.green : Color.red;
			DebugDistance.color = c;
			DebugDistance.text = Mathf.RoundToInt (agent.remainingDistance) + "m";
			DebugDistance.transform.position = Vector3.Lerp (transform.position, Player.transform.position, 0.5f);
			DebugDistance.transform.Translate (Vector3.up * 2);
			Debug.DrawLine (transform.position, Player.transform.position, c);
		}
		#endif
		if (isAlive) {
			AC.SetFloat ("AgentSpeed", agent.velocity.magnitude);
			if (Player) {
				agent.SetDestination (Player.transform.position);
				if (agent.remainingDistance < 2) {
					SO.Score -= Time.deltaTime;
				}
				if (Timer < 0) {
					if (agent.remainingDistance < 2) {
						SO.Score -= 10;
						AC.Play ("Attack0");
						Player.Pause ();
						isAttacking = false;
						Timer = 0.5f;
					}
				} else if (isAttacking) {
					Timer -= Time.deltaTime;
				}
			}
		}
	}

	public void Continue () {
		isAttacking = true;
	}

	public void SendAttack () {
		if (agent.remainingDistance <= 2) {
			isAttacking = false;
			Player.Die ();
			agent.Stop ();
		}
	}

	public void Step () {
		AS.PlayOneShot (FootSteps [Random.Range (0, FootSteps.Length)]);
	}

	public void GameOver() {
		Destroy (agent);
		Destroy (this);
	}

	public void Die () {
		AC.Play ("Death");
		gameObject.tag = "Untagged";
		GameObject[] othereEnemies = GameObject.FindGameObjectsWithTag ("Enemy");
		try {
			othereEnemies [Random.Range (0, othereEnemies.Length)].SendMessage ("AttackPlayer");
		} catch {
			StartCoroutine(Player.FindTargetPoint());
		}
		SO.SendScore ();
		isAlive = false;
	}

	public void Disable() {
		GameObject.Find ("BackMusic").GetComponent <AudioSource>().Pause ();

	}

	public void Enable() {
		GameObject.Find ("BackMusic").GetComponent <AudioSource>().UnPause ();
	}

	public void AttackPlayer () {
		isAttacking = true;
		Player = FindObjectOfType<KnightController> ();
		try {
			agent.SetDestination (Player.transform.position);
			Player.UpdateTarget (transform);
		} catch {
			Debug.Log ("Either agent or Player are destoyed.");
		}
	}
}