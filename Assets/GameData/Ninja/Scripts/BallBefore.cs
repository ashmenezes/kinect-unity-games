﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBefore : MonoBehaviour {
	public GameObject BrokenBullet;
	//private GameObject target;
	//public ParticleSystem fire;
	// Use this for initialization
	void Start () {
		Destroy (gameObject,15);
		//target = GameObject.Find ("Katana");
		//fire.Play;
	}
	
	// Update is called once per frame
	void Update () {
		//this.transform.LookAt (target.transform.position);
		//transform.localScale += new Vector3(1F, 1F, 1F);  //I need this later
	}

	void OnCollisionEnter (Collision other){
		//Debug.Log ("Collision of DestroyCube");
		if (other.gameObject.name == "Sword_joint" )
			DestroyMe ();
		else if (other.gameObject.name == "Body")
			DestroyMe ();
		//fire.Stop;

	}

	public void DestroyMe(){
		////Music.Play ();
		//Debug.Log ("Destroy me");
		Destroy (gameObject);

		//ps.Play();
		Instantiate (BrokenBullet, transform.position, transform.rotation);
		//StartCoroutine ("explosion");
	}

}
