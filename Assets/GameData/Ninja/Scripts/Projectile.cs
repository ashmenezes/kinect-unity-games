﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Effects;

public class Projectile : MonoBehaviour {

	void OnCollisionEnter(Collision col) {
		if (col.gameObject.name == "Sword_joint") {
			FindObjectOfType<ParticleSystemMultiplier> ().Explode(col.contacts[0].point);
			Destroy (gameObject);
		} else {
			Destroy (gameObject, 2);
			Destroy (this);
		}
	}

}
	