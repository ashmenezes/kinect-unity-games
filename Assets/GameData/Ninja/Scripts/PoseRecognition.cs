﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PoseRecognition : MonoBehaviour {


	public List<Pose> Poses = new List<Pose> ();
	public int PoseIndex;
	[HideInInspector]
	public bool ListenToPoses = true;
	[HideInInspector]
	public bool m_ControllerisAlive = true;

	public UnityEngine.UI.Text CalibrationProgress;
	public UnityEngine.UI.Text InstructionField;

	public GameObject m_Controller;
	LevelController m_LevelController;
	public bool Calibrated = false;
	public bool Calibrating = false;
	public float Timer = 10;
	public TextMesh DebugText;

	void Start () {
		m_LevelController = FindObjectOfType<LevelController> ();
	}

	void Update () {
		if (!Calibrated && Calibrating) {
			if (Timer > 0) {
				Timer -= Time.unscaledDeltaTime;
				if (Input.GetKeyDown (KeyCode.Return) || Timer < 0) {
					Timer = 0;
					Calibrating = CalibratePose ();
					if (!Calibrating) {
						FinishCalibration ();
					}
				}
				CalibrationProgress.text = "Record of pose in " + Mathf.Round (Timer * 100f) / 100f + " sec";
			}
		}
		if (!m_LevelController.IsPaused) {
			if (ListenToPoses && m_ControllerisAlive) {
				for (int i = 0; i < Poses.Count; i++) {
					if (Poses [i].isPoseCorrect ()) {
						if (!DebugText) {
							Debug.Log (Poses [i].Name + " is detected");
						} else {
							DebugText.text = Poses [i].Name + " is detected";
						}
						m_Controller.SendMessage (Poses [i].Name);
						return;
					}
				}
				if (DebugText) {
					DebugText.text = "Nothing is detected";
				}
			}
		}
	}

	public void RestartCalibration () {
		PoseIndex = 0;
		Calibrating = true;
		Calibrated = false;
		FindObjectOfType<LevelController> ().Pause ();
		FindObjectOfType<ScoreWrapper> ().HideGameOverScreen ();
		CalibrationProgress.transform.parent.gameObject.SetActive (true);
		Timer = 5;
		InstructionField.text = "Stand in " + Poses [PoseIndex].Name + " pose";
	}

	public void FinishCalibration () {
		Calibrated = true;
		FindObjectOfType<LevelController> ().Resume ();
		CalibrationProgress.transform.parent.gameObject.SetActive (false);
	}

	public void StopCalibration () {
		PoseIndex = 0;
		Calibrating = false;
		Calibrated = false;
		CalibrationProgress.transform.parent.gameObject.SetActive (true);
		Timer = 5;
		InstructionField.text = "Stand in front of the kinect to start.";
	}

	public bool CalibratePose () {
		Poses [PoseIndex].CalibratePoints ();
		PoseIndex++;
		try {
			InstructionField.text = "Stand in " + Poses [PoseIndex].Name + " pose";
		} catch  {
			InstructionField.text = "Calibration finished";
		}
		Timer = 5;
		return PoseIndex < Poses.Count;
	}

	public bool CalibrateParticularPose (int PoseIndex) {
		Poses [PoseIndex].CalibratePoints ();
		InstructionField.text = "Pose " + Poses [PoseIndex].Name + " is calibrated";
		return PoseIndex < Poses.Count - 1;
	}
}
