﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


/// <summary>
/// This is needed to change position of the main camera once a level is loaded;
/// </summary>
public class KnightController : MonoBehaviour {

	public Transform PreferedPoint;
	public Transform TargetPoint;
	public float MaxDistance = 10;
	public float MinDistance = 5;
	public float damping = 10;


	public Transform MainCamera;

	[Header ("Particles")]
	public ParticleSystem Shield;
	public ParticleSystem Sword;

	[Header ("Sounds")]
	public AudioClip DefendSound;
	public AudioClip[] AttackSound;
	public AudioClip[] FootSteps;

	public bool Alive = true;
	bool Protected = false;
	float[] velTime;
	LevelController PC;
	AudioSource[] AllASs;
	AudioSource AS;
	Animator AC;
	NavMeshAgent agent;
	Vector3 vel;

	void Start () {
		PC = FindObjectOfType <LevelController> ();
		AS = GetComponent <AudioSource> ();
		AllASs = FindObjectsOfType<AudioSource> ();
		velTime = new float[AllASs.Length];
		agent = GetComponent <NavMeshAgent> ();
		AC = GetComponent <Animator> ();
		StartBattle ();
	}

	void Update () {
		if (!PC.IsPaused) {
			AC.SetFloat ("AgentSpeed", agent.velocity.magnitude);
			MainCamera.position = Vector3.SmoothDamp (MainCamera.position, PreferedPoint.position, ref vel, 0.1f);
			var desiredRotQ = Quaternion.Euler (PreferedPoint.eulerAngles.x, PreferedPoint.eulerAngles.y, PreferedPoint.eulerAngles.z);
			MainCamera.rotation = Quaternion.Lerp (MainCamera.rotation, desiredRotQ, Time.deltaTime * damping);
			if (isProjectileClose ()) {
				Time.timeScale = 0.1f;
			} else {
				Time.timeScale = 1;
			}
			for (int i = 0; i < AllASs.Length; i++) {
				AllASs [i].pitch = Mathf.SmoothDamp (AllASs [i].pitch, Time.timeScale, ref velTime [i], 0.1f);
			}
			if (TargetPoint) {
				agent.SetDestination (TargetPoint.position);
			}
		}
	}

	public void Pause () {
		#if UNITY_5_5_0
		agent.Stop ();
		#else
		agent.isStopped = true;
		#endif
		Debug.Log ("Pausing!");
	}

	public void Continue () {
		#if UNITY_5_5_0
		agent.Resume ();
		#else
		agent.isStopped = false;
		#endif
		Protected = false;
		Debug.Log ("Continuing!");
		FindObjectOfType<PoseRecognition> ().ListenToPoses = true;
	}

	public void UpdateTarget (Transform target) {
		TargetPoint = target;
		agent.SetDestination (TargetPoint.position);
	}

	public void SendAttack () {
		if (agent.remainingDistance <= 3.5f) {
			TargetPoint.SendMessage ("Die");
		}
	}

	public void Attack () {
		Protected = false;
		FindObjectOfType<PoseRecognition> ().ListenToPoses = false;
		AC.Play ("Attack0");
		AS.PlayOneShot (AttackSound [Random.Range (0, AttackSound.Length)]);
		Sword.Play ();
		Pause ();
	}

	public void Defend () {
		Protected = true;
		FindObjectOfType<PoseRecognition> ().ListenToPoses = false;
		AC.Play ("Shield");
		AS.PlayOneShot (DefendSound);
		Shield.Play ();
		Pause ();
	}

	public void Walk () {
		agent.speed = 1.15f;
	}

	public void Run () {
		agent.speed = 3.5f;
	}

	public void Step () {
		AS.PlayOneShot (FootSteps [Random.Range (0, FootSteps.Length)]);
	}

	public void Die () {
		if (!Protected) {
			Pause ();
			AC.Play ("Death");
			FindObjectOfType<PoseRecognition> ().m_ControllerisAlive = false;
		} else {
			AC.Play ("Impact");
		}
	}

	public void GameOver () {
		FindObjectOfType<LevelController> ().GameOver ("GAME OVER", "You died");
		Destroy (agent);
		Destroy (this);
	}

	public IEnumerator FindTargetPoint () {
		TargetPoint = GameObject.Find ("TargetPoint").transform;
		yield return new WaitForSeconds (3);
		FindObjectOfType<LevelController> ().GameOver ("GAMEOVER", "You defended the castle!");
	}

	public void StartBattle () {
		EnemyController[] othereEnemies = FindObjectsOfType <EnemyController> ();
		othereEnemies [Random.Range (0, othereEnemies.Length)].AttackPlayer ();
	}

	public void Disable () {

	}

	public void Enable () {
		
	}

	public bool isProjectileClose () {
		GameObject[] gos = GameObject.FindGameObjectsWithTag ("Projectile");
		foreach (var item in gos) {
			float distance = Vector3.Distance (item.transform.position, PreferedPoint.position);
			if (distance < 1) {
				item.tag = "Untagged";
			}
			if (distance > MinDistance && distance < MaxDistance) {
				return true;
			}
		}
		return false;
	}

	public GameObject GetClosestEnemy () {
		GameObject[] gos = GameObject.FindGameObjectsWithTag ("Enemy");
		foreach (var item in gos) {
			float distance = Vector3.Distance (item.transform.position, transform.position);
			if (distance < 2) {
				return item;
			}
		}
		return null;
	}
}
