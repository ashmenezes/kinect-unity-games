﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Pauses game if player is lost.
/// </summary>
public class PauseController : MonoBehaviour {

	public  List<GameObject> Controllers = new List<GameObject> ();
	public GameObject ScoreInterface;
	public GameObject InstrutionWindow;

	/// <summary>
	/// Pause when false, Unpause when true;
	/// </summary>
	public bool activeMod;

	void Start () {
		Pause ();
	}
		
	public void Pause () {
		//throw new UnityException ("Pause Not yet implemented");
		foreach (var item in Controllers) {
			item.SendMessage ("Disable", SendMessageOptions.DontRequireReceiver);
		}
		if (ScoreInterface) {
			ScoreInterface.SetActive (false);
		}
		activeMod = false;
		InstrutionWindow.SetActive (true);
		Time.timeScale = 0;

	}

	public void Resume () {
		//throw new UnityException ("Resume Not yet implemented");
		foreach (var item in Controllers) {
			item.SendMessage ("Enable");
		}

		activeMod = true;
		if (ScoreInterface) {
			InstrutionWindow.SetActive (false);
		}
		ScoreInterface.SetActive (true);
		Time.timeScale = 1;
	}
}
