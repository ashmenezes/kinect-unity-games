﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bubble : MonoBehaviour {

	[Tooltip("Scene to load, when bubble is poped")]
	[TextArea(3,5)]
	public string LevelName = "<Scene name>";
	[Tooltip("Pop sound")]
	public AudioClip PopSound;
    [Tooltip("Bubble drops, can be null.")]
    public ParticleSystem BubbleParticles;
    [Tooltip("Force required to pop the bubble")]
	public float MinmumImuplseToPop = 1;
	public float LastImpusle;

	public TextMesh DebugText;

	Vector3 vel = Vector3.zero;

	void Start() {
		if (DebugText) {
			DebugText.transform.parent = null;
		}
	}

	void Update() {
		if (DebugText) {
			DebugText.transform.position = Vector3.SmoothDamp (DebugText.transform.position, transform.position, ref vel, 0.1f);
		}
		if (LastImpusle > 0) {
			LastImpusle -= Time.deltaTime;
		} else if (LastImpusle < 0){
			LastImpusle = 0;
		}
	}

	public void OnCollisionEnter(Collision col) {
		if (DebugText) {
			DebugText.text = "\t\t\t" + gameObject.name + " detected impulse. \n\t\t\tIt was " + col.impulse.magnitude + ", \n\t\t\trequired " + MinmumImuplseToPop;
		}
		if (LastImpusle == 0) {
			LastImpusle = col.impulse.magnitude;
		}
		if (col.impulse.magnitude > MinmumImuplseToPop) {
            Pop ();
        }
	}

	public void Pop() {
        FindObjectOfType<KinectManager>().TwoUsers = gameObject.name == "Godzilla" ? true : false;
        if (BubbleParticles)
        {
            BubbleParticles.transform.position = transform.position;
            BubbleParticles.Play();
        }
        //ps.SetActive(true);
        FindObjectOfType<LevelsManager> ().LoadScene (LevelName);
		if (PopSound) {
			GameObject.Find ("GlobalShortAS").GetComponent<AudioSource> ().PlayOneShot (PopSound);
		} else {
			Debug.Log (gameObject.name + " has no pop sound. Ignoring PlayOneShot() call.");
		}
		Destroy (gameObject);
    }

}
