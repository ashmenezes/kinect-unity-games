﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelsManager : MonoBehaviour {

	FadeInOut m_FadeInOut;
	[HideInInspector]
	public string SelectedScene;
	[HideInInspector]
	public bool Loading;

	void Start() {
		if (FindObjectsOfType<LevelsManager> ().Length > 1) {
			Destroy (gameObject);
		} else {
			m_FadeInOut = FindObjectOfType<FadeInOut> ();
			//DontDestroyOnLoad (gameObject);
		}
	}

	public void LoadScene(string Scene) {
		SceneManager.LoadScene (Scene);
	}
}
