﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeInOut : MonoBehaviour {

	[HideInInspector]
	public Material FadeImage;
	public float targetFade;

	float vel = 0.0f;

	void Start() {
		FadeImage = GetComponent <Image> ().material;
		FadeImage.color = new Color (1,1,1,1);
		targetFade = 0;
	}

	void Update () {
		FadeImage.color = new Color(1,1,1,Mathf.SmoothDamp (FadeImage.color.a,targetFade,ref vel,0.1f,0.7f));
	}

	/// <summary>
	/// Fade out or in based on fade parameter.
	/// </summary>
	/// <param name="fade">If set to <c>true</c> fade, then it will fade in, otherwise fade out.</param>
	public void Fade(bool fade) {
		if (fade) {
			targetFade = 1;
		} else {
			targetFade = 0;
		}
	}
}
