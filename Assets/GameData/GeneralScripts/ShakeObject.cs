﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeObject : MonoBehaviour {

	public float AmountOfShaking = 0.1f;
	public float Decay = 0.007f;
	public bool Automatic;

	[HideInInspector]
	public float shake_intensity;
	[HideInInspector]
	public float shake_decay;
	[HideInInspector]
	public Vector3 originPosition;
	[HideInInspector]
	public Quaternion originRotation;

	// Use this for initialization
	void Start () {
		originPosition = transform.localPosition;
		originRotation = transform.localRotation;
		if (Automatic) {
			Shake ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		Shaking ();
	}

	public void Shaking() {
		if (shake_intensity > 0) {
			transform.localPosition = originPosition + UnityEngine.Random.insideUnitSphere * shake_intensity;
			//transform.localRotation = new Quaternion (originRotation.x + UnityEngine.Random.Range (-shake_intensity, shake_intensity) * 0.2f, originRotation.y + UnityEngine.Random.Range (-shake_intensity, shake_intensity) * 0.2f, originRotation.z + UnityEngine.Random.Range (-shake_intensity, shake_intensity) * 0.2f, originRotation.w + UnityEngine.Random.Range (-shake_intensity, shake_intensity) * 0.2f);
			shake_intensity -= shake_decay;
		}
	}

	public void Shake()
	{
		shake_intensity = AmountOfShaking;
		shake_decay = Decay;
	}
}
