﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerResult
{

	public string Name;
	public int Score;
	public float Time;

	public PlayerResult (string Name, int Score, float Time)
	{
		this.Name = Name;
		this.Score = Score;
		this.Time = Time;
	}

}
