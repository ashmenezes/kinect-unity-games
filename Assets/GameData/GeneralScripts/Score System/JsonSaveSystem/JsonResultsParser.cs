﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class JsonResultsParser : MonoBehaviour {

	[Serializable]
	public class JsonResultsParserChild {
		public List<PlayerResult> PlayerResults = new List<PlayerResult> ();
	}

	public Transform TableContent;
	public GameObject PlayerResultField;
	public JsonResultsParserChild mJsonResultsParserChild;
	public ScoreManager mScoreManager;
	LevelController levelController;

	public GameObject Database;

	string Path;
	PlayerResult CurrentPlayerResult;

	void Start () {
		this.Path = UnityEngine.SceneManagement.SceneManager.GetActiveScene ().name + "/GameResults";
		levelController = FindObjectOfType<LevelController> ();
		mScoreManager = FindObjectOfType<ScoreManager> ();
		LoadAllResults ();
	}

	public void LoadAllResults () {
		mJsonResultsParserChild = JsonUtility.FromJson<JsonResultsParserChild> (LoadTextFile ());
		Transform[] oldTable = TableContent.GetComponentsInChildren <Transform> ();
		foreach (var item in oldTable) {
			if (item != TableContent) {
				Destroy (item.gameObject);
			}
		}
		mJsonResultsParserChild.PlayerResults.Sort ((x, y) => y.Score.CompareTo (x.Score));
		foreach (var item in mJsonResultsParserChild.PlayerResults) {
			GameObject tempPRF = Instantiate (PlayerResultField, TableContent);
			Text[] texts = tempPRF.GetComponentsInChildren <Text> ();
			if (item.Name == CurrentPlayerResult.Name) {
				foreach (var TextItem in texts) {
					TextItem.color = new Color (242f / 255f, 38 / 255f, 19 / 255f, 1f);
				}
			}
			texts [0].text = item.Name;
			texts [1].text = item.Score.ToString ();
			if (item.Time != 0.0f) {
				texts [2].text = item.Time + " sec";
			} else {
				Destroy (texts [2].gameObject);
			}
		}
	}

	public void SaveResult (string Name) {
		if (Name != "") {
			CurrentPlayerResult = new PlayerResult (Name, Mathf.RoundToInt (mScoreManager.TotalScore), Mathf.Round (levelController.TimeLeft));
			mJsonResultsParserChild.PlayerResults.Add (CurrentPlayerResult);
			SaveJson (JsonUtility.ToJson (mJsonResultsParserChild));
		}
		LoadAllResults ();
	}

	public void SaveResultG (string Name) {
		if (Name != "") {
			CurrentPlayerResult = new PlayerResult (Name, FindObjectOfType<GameOverG> ().WinScore, Mathf.Round (levelController.TimeLeft));
			mJsonResultsParserChild.PlayerResults.Add (CurrentPlayerResult);
			SaveJson (JsonUtility.ToJson (mJsonResultsParserChild));
		}
		try {
			GameObject.Find ("Player1Victory").SetActive (false);
		} catch {
		}
		try {
			GameObject.Find ("Player2Victory").SetActive (false);
		} catch {
		}
		try {
			GameObject.Find ("Tie").SetActive (false);
		} catch {
		}
		GameObject.Find ("WinScore").SetActive (false);
		GameObject.Find ("Highscore").SetActive (false);
		GameObject.Find ("AddScoreWindow").SetActive (false);
		GameObject.Find ("DatabaseWindow").SetActive (true);
		LoadAllResults ();
	}

	public void SaveJson (string json) {
		string path = null;
		//Debug.Log ("Saving...\n" + json);
		#if UNITY_EDITOR
		path = "Assets/Resources/" + Path + ".json";
		#else
		path = "MyGame_Data/Resources/ItemInfo.json"
		#endif
		using (FileStream fs = new FileStream (path, FileMode.Create)) {
			using (StreamWriter writer = new StreamWriter (fs)) {
				writer.Write (json);
			}
		}
		#if UNITY_EDITOR
		UnityEditor.AssetDatabase.Refresh ();
		#endif
	}

	string LoadTextFile () {
		string data = Resources.Load<TextAsset> (Path).text;
		//Debug.Log ("Reading..." + data);
		return data;
	}

}
