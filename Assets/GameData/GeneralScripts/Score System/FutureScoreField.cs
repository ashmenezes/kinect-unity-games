﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script is used to display score that you get imidietly.
/// </summary>
public class FutureScoreField : MonoBehaviour {

	Vector3 Target;
	float Score;
	UnityEngine.UI.Text ScoreField;
	Vector3 FutureScoreFieldVel = Vector3.zero;
	float ColorVel = 0.0f;

	void Start () {
		ScoreField = GetComponent <UnityEngine.UI.Text > ();
		ScoreField.text = "+" + Score;
	}

	void Update () {
		transform.position = Vector3.SmoothDamp (transform.position, Target, ref FutureScoreFieldVel, 0.7f);
		ScoreField.color = new Color (1, 1, 1, Mathf.SmoothDamp (ScoreField.color.a, 0, ref ColorVel, 0.7f));
	}

	/// <summary>
	/// Initialises Field
	/// </summary>
	/// <param name="Target">Point to reach.</param>
	/// <param name="Score">Score to display.</param>
	public void SetField (Vector3 Target, float Score) {
		this.Target = Target;
		this.Score = Score;
		ScoreField = GetComponent <UnityEngine.UI.Text > ();
		ScoreField.text = Score>0 ? "+" + Score : "-" + Score;
		transform.localPosition = Vector3.zero;
		ScoreField.color = new Color (1, 1, 1, 1);
		Destroy (gameObject, 1);
	}
}
