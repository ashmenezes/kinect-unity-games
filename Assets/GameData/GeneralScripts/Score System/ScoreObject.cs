﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Sends score info when an object is destoyed.
/// </summary>
public class ScoreObject : MonoBehaviour {

	public float Score = 100;

	void Update() {
		Score = Mathf.Clamp (Score,0,100);
	}

	/// <summary>
	/// Sends the score and display input.
	/// </summary>
	public void SendScore () {
		try {
			FindObjectOfType<ScoreManager> ().UpdateScore (Score);
		} catch {
			Debug.Log ("Something bad is happening!");
		}
	}

	/// <summary>
	/// Sends the score, but doesn't display.
	/// </summary>
	public void SendScoreDontShow () {
		FindObjectOfType<ScoreManager> ().UpdateScore (Score, false);
	}

	/// <summary>
	/// Raises the destroy event.
	/// </summary>
	void OnDestroy () {
		//SendScore ();
	}

	#if UNITY_EDITOR
	void OnGUI () {
		if (Input.GetKey (KeyCode.E)) {
			if (GUILayout.Button ("SendScore()")) {
				SendScore ();
			}
			if (GUILayout.Button ("SendScoreDontShow()")) {
				SendScoreDontShow ();
			}
		}
	}
	#endif
}
