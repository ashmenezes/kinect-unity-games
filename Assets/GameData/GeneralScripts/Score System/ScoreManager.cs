﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Store score of the current session and saves best result
/// </summary>
public class ScoreManager : MonoBehaviour {

	public float ScoreToDisplay;
	public float TotalScore;
	public string Suffix = "Score: ";
	public string BestScoreSuffix = "Best Score: ";
	public AudioClip ScoreSound;

	AudioSource AS;
	Text BestScoreField;
	public Text ScoreField;
	Text m_FutureScoreField;

	bool WasItNewHighScore;
	float ScoreVel = 0.0f;

	void Start () {
		Debug.Log ("Calling Start in ScoreManager from " + gameObject.name);
		AS = gameObject.AddComponent <AudioSource> ();
		AS.volume = 0.1f;
		try {
			if (!ScoreField) {
				ScoreField = GameObject.Find ("ScoreField").GetComponent <Text> ();
			}
			BestScoreField = GameObject.Find ("BestScoreField").GetComponent <Text> ();
		} catch {
			Debug.Log ("[DR.HAX PRODUCTION] - Either objects with ScoreField or BestScoreField are not found. Make sure that there are in the scene and have the same names.");
			Debug.Break ();
		}
		if (GameObject.Find ("FutureScoreField")) {
			m_FutureScoreField = GameObject.Find ("FutureScoreField").GetComponent <Text> ();
			m_FutureScoreField.color = new Color (1, 1, 1, 0);
		}
	}

	void Update () {
		ScoreToDisplay = Mathf.SmoothDamp (ScoreToDisplay, TotalScore, ref ScoreVel, 0.1f);
		ScoreField.text = Suffix + Mathf.RoundToInt (ScoreToDisplay);
		BestScoreField.text = BestScoreSuffix + PlayerPrefs.GetInt (SceneManager.GetActiveScene ().name);
	}

	/// <summary>
	/// Updates the score field, spawns <see cref="FutureScoreField"/> and play sound if assigned.
	/// </summary>
	/// <param name="Score">Score to add and display.</param>
	/// <param name="Show">If set to <c>true</c> displaies input and plays sound if assigned.</param>
	public void UpdateScore (float Score, bool Show = true) {
		//Debug.Log ("Calling UpdateScore in ScoreManager from " + gameObject.name);
		TotalScore += Score;
		if (Show) {
			GameObject tempgo = Instantiate (m_FutureScoreField, transform.position, Quaternion.identity, transform).gameObject;
			tempgo.GetComponent <FutureScoreField> ().SetField (ScoreField.transform.position, Score);
			if (ScoreSound) {
				AS.PlayOneShot (ScoreSound);
			}
		}
	}

	public void SaveBestResult () {
		Debug.Log ("Calling SaveBestResult in ScoreManager from " + gameObject.name);
		int tempbestresult = 0;
		string BestScore = SceneManager.GetActiveScene ().name;
		if (PlayerPrefs.HasKey (BestScore)) {
			tempbestresult = PlayerPrefs.GetInt (BestScore);
			if (TotalScore > tempbestresult) {
				WasItNewHighScore = true;
				PlayerPrefs.SetInt (BestScore, (int)TotalScore);
			}
		} else {
			WasItNewHighScore = true;
			PlayerPrefs.SetInt (BestScore, (int)TotalScore);
		}
	}

	public int GetBestResult() {
		return PlayerPrefs.GetInt (SceneManager.GetActiveScene ().name, 0);
	}

	public bool isItNewHighScore() {
		return WasItNewHighScore;
	}

	#if UNITY_EDITOR
	void OnGUI () {
		if (Input.GetKey (KeyCode.Alpha1)) {
			if (GUILayout.Button ("SaveBestResult()")) {
				SaveBestResult ();
			}
		}
	}
	#endif
}
