﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


/// <summary>
/// Game over, if the time is over or player hit anything.
/// </summary>
public class GameOverController : MonoBehaviour
{
	
	public GameObject gameOverWindow;

	public float timeLeft;
	public Slider _slider;

	public PauseController checkPauseMod;

	Text timeLeftText;
	public string timeLeftSuffix = "Time Left: ";

	// Use this for initialization
	void Start ()
	{
		checkPauseMod = FindObjectOfType<PauseController> ();
		timeLeftText = GameObject.Find ("TimeLeftField").GetComponent <Text> ();
	}

	// Update is called once per frame
	void Update ()
	{
		if (checkPauseMod.activeMod && timeLeft > 0) {
			timeLeft -= Time.deltaTime;
			if (timeLeft <= 0) {
				timeLeft = 0;
				FinishTheGame ();
			}
		}
		timeLeftText.text = timeLeftSuffix + Mathf.Round (timeLeft * 100f) / 100f + " seconds";
		if (Input.GetMouseButtonDown (1)) {
			RestartGame ();
		}
	}

	public void FinishTheGame ()
	{
		try {
			FindObjectOfType<ScoreManager> ().SaveBestResult ();
		} catch {
			Debug.Log ("There is no score manager in the scene.");
		}
		checkPauseMod.Pause ();
		checkPauseMod.InstrutionWindow.SetActive (false);
		gameOverWindow.SetActive (true);

	}

	public void RestartGame ()
	{
		if (!checkPauseMod.activeMod) {
			SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
		}
	}
}