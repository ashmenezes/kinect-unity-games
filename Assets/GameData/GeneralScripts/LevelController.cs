﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour {

	/// <summary>
	/// Assign here controller that has Enable and Disable methods for blocking controling
	/// </summary>
	[Tooltip ("Assign here controller that has Enable and Disable methods for blocking controling")]
	public  List<GameObject> Controllers = new List<GameObject> ();

	public float TimeLeft;
	public string TimeLeftSuffix = "Time Left: ";
	public string ResultWhenTImeIsUp = "Time is up";

	[Header ("Level info")]
	public string Objective = "Objective: Defeat all soldiers";

	GameObject ScoreUI;
	public Slider TimeSlider;
	Text TimeLeftField;

	/// <summary>
	/// Indicator of pause
	/// </summary>
	bool isPaused = false;
	/// <summary>
	/// It blocks methods Pause() and Resume()
	/// </summary>
	bool isGameOver = false;
	bool isGameStarted = false;

	public bool IsPaused {
		get {
			return isPaused;
		}
	}

	public bool IsGameOver {
		get {
			return isGameOver;
		}
	}

	void Start () {
		TimeLeftField = GameObject.Find ("TimeLeftField").GetComponent <Text> ();
		ScoreUI = GameObject.Find ("ScoreUIChild");
		Pause ();
		FindObjectOfType<ScoreWrapper> ().HideGameOverScreen ();
	}

	void Update () {
		if (!isPaused && !isGameOver) {
			TimeLeft -= Time.deltaTime;
			if (TimeLeft < 0) {
				TimeLeft = 0;
				GameOver (ResultWhenTImeIsUp);
			}
			TimeLeftField.text = TimeLeftSuffix + Mathf.Round (TimeLeft * 100f) / 100f + " sec";
		} else if (TimeSlider) {
			TimeLeft = TimeSlider.value;
		}
		ScoreUI.SetActive (!isPaused && !isGameOver);
	}

	public void StartTheGame () {
		try {
			FindObjectOfType<PoseRecognition> ().RestartCalibration ();
			return;
		} catch {
			Debug.Log ("PoseRecognition is not found");
		}
		Resume ();
	}

	public void Pause () {
		if (!isGameOver) {
			foreach (var item in Controllers) {
				item.SendMessage ("Disable", SendMessageOptions.DontRequireReceiver);
			}
			Time.timeScale = 0.1f;
			isPaused = true;
			try {
				FindObjectOfType<ScoreWrapper> ().Initialize ();
				FindObjectOfType<ScoreWrapper> ().DisplayScore (FindObjectOfType<ScoreManager> (), this, "PAUSE", Objective);
			} catch {
				Debug.Log ("ScoreWrapper not found");
				FindObjectOfType<PauseController> ().Pause ();
			}
		}
	}

	public void Resume () {
		if (!isGameOver) {
			foreach (var item in Controllers) {
				item.SendMessage ("Enable");
			}
			if (TimeSlider) {
				Destroy (TimeSlider);
			}
			Time.timeScale = 1;
			isPaused = false;
			try {
				FindObjectOfType<ScoreWrapper> ().HideGameOverScreen ();
			} catch {
				Debug.Log ("ScoreWrapper not found");
				FindObjectOfType<PauseController> ().Resume ();
			}
		}
	}

	public void Restart () {
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}

	public void GameOver (string Title = "Missing Title", string ResultOfTheGame = "Missing Result") {
		if (!isGameOver) {
			Pause ();
			isGameOver = true;
			ScoreManager temp_ScoreManager;
			ScoreManager[] temp_m_ScoreManagers = FindObjectsOfType<ScoreManager> ();
			if (temp_m_ScoreManagers.Length > 1) {
				if (temp_m_ScoreManagers [0].TotalScore > temp_m_ScoreManagers [1].TotalScore) {
					temp_ScoreManager = temp_m_ScoreManagers [0];
					temp_m_ScoreManagers [0].SaveBestResult ();
					ResultOfTheGame = ResultOfTheGame == "You died" || ResultOfTheGame == "You didn't survive..." ? ResultOfTheGame : "P1 WON";
				} else {
					temp_ScoreManager = temp_m_ScoreManagers [1];
					temp_m_ScoreManagers [1].SaveBestResult ();
					ResultOfTheGame = ResultOfTheGame == "You died" || ResultOfTheGame == "You didn't survive..." ? ResultOfTheGame : "P2 WON";
				}
			} else {
				temp_m_ScoreManagers [0].SaveBestResult ();
				temp_ScoreManager = temp_m_ScoreManagers [0];
				ResultOfTheGame = ResultOfTheGame == "You died" || ResultOfTheGame == "You didn't survive..." ? ResultOfTheGame : "You WON!";
			}
			FindObjectOfType<ScoreWrapper> ().DisplayScore (temp_ScoreManager, this, Title, ResultOfTheGame);
		}
	}
}