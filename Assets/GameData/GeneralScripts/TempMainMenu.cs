﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TempMainMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
		if (GameObject.FindObjectsOfType<TempMainMenu> ().Length > 1) {
			Destroy (gameObject);
		} else {
			DontDestroyOnLoad (gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.A)) {
			FindObjectOfType<LevelsManager> ().LoadScene ("MainMenu");
		}
	}
}
