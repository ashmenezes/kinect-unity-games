﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreWrapper : MonoBehaviour
{

	Text TitleField;
	Text ResultOfTheGame;
	Text YourTime;
	Text BestTime;
	Text YourScore;
	Text NewHighScore;
	Text BestScore;

	void Start ()
	{
		Initialize ();
	}

	public void Initialize ()
	{
		try {
			TitleField = GameObject.Find ("TitleField").GetComponent <Text> ();
			ResultOfTheGame = GameObject.Find ("ResultOfTheGame").GetComponent <Text> ();
			YourTime = GameObject.Find ("YourTime").GetComponent <Text> ();
			NewHighScore = GameObject.Find ("NewHighScore").GetComponent <Text> ();
			YourScore = GameObject.Find ("YourScore").GetComponent <Text> ();
		} catch {
			Debug.Log ("[DR.HAX PRODUCTION] - One of the UI elements are not found.");
		}
	}

	/// <summary>
	/// Displaies the score using info from temp_m_ScoreManager.
	/// </summary>
	/// <param name="temp_m_ScoreManager">Source of information about scores.</param>
	public void DisplayScore (ScoreManager temp_m_ScoreManager, LevelController temp_m_LevelController, string mTitle = "Missing Title", string ResultOfTheGame = "Missing ResultOfTheGame")
	{
		transform.GetChild (0).gameObject.SetActive (true);
		this.TitleField.text = mTitle;
		this.ResultOfTheGame.text = ResultOfTheGame;
		if (temp_m_ScoreManager && temp_m_LevelController) {
			this.YourTime.text = "Your Time: " + Mathf.Round (temp_m_LevelController.TimeLeft * 100f) / 100f + " sec";
			this.YourScore.text = "Your Score: " + Mathf.RoundToInt (temp_m_ScoreManager.TotalScore);
			NewHighScore.enabled = temp_m_ScoreManager.isItNewHighScore ();
		}
	}

	public void HideGameOverScreen ()
	{
		transform.GetChild (0).gameObject.SetActive (false);
	}
}
