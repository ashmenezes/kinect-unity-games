﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterPos : MonoBehaviour {

	public Transform Pos1;
	public Transform Pos2;
 
	void Update () {
		transform.position = Vector3.Lerp (Pos1.position,Pos2.position,0.5f);
	}
}
