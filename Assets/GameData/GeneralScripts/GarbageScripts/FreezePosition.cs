﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezePosition : MonoBehaviour {

	public bool FreezeX;
	public bool FreezeY;
	public bool FreezeZ;

	bool Freeze;
	public Transform FrozenPos;

	void Update () {
		if (Freeze) {
			transform.position = new Vector3 (
				FreezeX ? FrozenPos.position.x : transform.position.x,
				FreezeY ? FrozenPos.position.y : transform.position.y,
				FreezeZ ? FrozenPos.position.z : transform.position.z
			);
		}
	}

	public void Froze(Transform pos) {
		FrozenPos = pos;
		Freeze = true;
		transform.position = pos.position;
	}
}
