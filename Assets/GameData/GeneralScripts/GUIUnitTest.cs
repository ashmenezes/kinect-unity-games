﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIUnitTest : MonoBehaviour {

	Rect windowRect;

	[Header ("Assign them if you want to test it")]
	public PlayerContoller m_PlayerContoller;
	public KnightController m_KnightController;
	public PoseRecognition m_PoseRecognition;
	public LevelController m_LevelController;
	public Countdown m_Countdown;
	public GUISkin m_GUISkin;

	void OnGUI () {
//		if (m_GUISkin) {
//			GUI.skin = m_GUISkin;
//		}
		if (Input.GetKey (KeyCode.Q)) {
			windowRect =  new Rect(0, 0, Screen.width, Screen.height);
			windowRect = GUILayout.Window(0, windowRect, DoMyWindow, "Unit Tests");
		}
	}

	void DoMyWindow(int windowID) {
		GUILayout.BeginHorizontal ();
		if (m_PlayerContoller) {
			GUILayout.BeginVertical ("m_PlayerContoller");
			GUILayout.Box ("m_PlayerContoller");
			if (GUILayout.Button ("It needs methods for testing")) {

			}
			GUILayout.EndVertical ();
		}
		if (m_KnightController && m_KnightController.Alive) {
			GUILayout.BeginVertical ("m_KnightController");
			GUILayout.Box ("m_KnightController");
			if (GUILayout.Button ("Attack")) {
				m_KnightController.Attack ();
			}
			if (GUILayout.Button ("Defend")) {
				m_KnightController.Defend ();
			}
			if (GUILayout.Button ("Walk")) {
				m_KnightController.Walk ();
			}
			if (GUILayout.Button ("Run")) {
				m_KnightController.Run ();
			}
			GUILayout.EndVertical ();
		}
		if (m_PoseRecognition) {
			GUILayout.BeginVertical ("m_PoseRecognition");
			GUILayout.Box ("m_PoseRecognition");
			if (GUILayout.Button ("RestartCalibration()")) {
				m_PoseRecognition.RestartCalibration ();
			}
			for (int i = 0; i < m_PoseRecognition.Poses.Count; i++) {
				if (GUILayout.Button ("Calibrate " + m_PoseRecognition.Poses [i].Name)) {
					m_PoseRecognition.CalibrateParticularPose (i);
				}
			}
			if (GUILayout.Button ("FinishCalibration()")) {
				m_PoseRecognition.FinishCalibration ();
			}
			if (GUILayout.Button ("StopCalibration()")) {
				m_PoseRecognition.StopCalibration ();
			}
			GUILayout.EndVertical ();
		}
		if (m_LevelController) {
			GUILayout.BeginVertical ("m_LevelController");
			GUILayout.Box ("m_LevelController");
			if (GUILayout.Button ("StartTheGame()")) {
				m_LevelController.StartTheGame ();
			}
			if (GUILayout.Button ("Pause()")) {
				m_LevelController.Pause ();
			}
			if (GUILayout.Button ("Resume()")) {
				m_LevelController.Resume ();
			}
			if (GUILayout.Button ("GameOver()")) {
				m_LevelController.GameOver ();
			}
			if (GUILayout.Button ("Restart()")) {
				m_LevelController.Restart ();
			}
			GUILayout.EndVertical ();
		}
		if (m_Countdown) {
			GUILayout.BeginVertical ("m_Countdown");
			GUILayout.Box ("m_Countdown");
			if (GUILayout.Button ("StartTheGame()")) {
				m_Countdown.StartTheGame ();
			}
			if (GUILayout.Button ("Pause()")) {
				m_Countdown.Pause ();
			}
			if (GUILayout.Button ("Resume()")) {
				m_Countdown.Resume ();
			}
			if (GUILayout.Button ("Resume(true)")) {
				m_Countdown.Resume (true);
			}
			if (GUILayout.Button ("GameOver()")) {
				m_Countdown.GameOver ();
			}
			if (GUILayout.Button ("Restart()")) {
				m_Countdown.Restart ();
			}
			GUILayout.EndVertical ();
		}
		GUILayout.EndHorizontal ();
		if (GUILayout.Button ("MainMenu()")) {
			UnityEngine.SceneManagement.SceneManager.LoadScene ("MainMenu");
		}
	}
}
