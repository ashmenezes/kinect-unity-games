﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Spawns specified object at current position.
/// </summary>
public class BallSpawner : MonoBehaviour {

    public GameObject b;


	/// <summary>
	/// Spawns an object.
	/// </summary>
    void SpawnBall()
    {
        Instantiate(b, transform.position, Quaternion.identity);
    }

    void OnGUI(){
        if (Input.GetKey((KeyCode.Q)))
        {
            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("SpawnBall()"))
            {
                SpawnBall();
            }

            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
        }
    }
}
