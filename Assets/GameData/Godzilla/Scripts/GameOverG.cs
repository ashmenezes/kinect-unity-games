﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverG : MonoBehaviour {

	public Text P1Win;
	public Text P2Win;
	public Text tie;
	public Text score;
	public Text nhs;
	public int WinScore;
	//public UnityEngine.UI.Text WinnerText;
	//public UnityEngine.UI.Text ScoreText;

    // Use this for initialization
    void OnEnable () {
		int p1Score = Mathf.RoundToInt(GameObject.Find ("ScoreManager" + 1).GetComponent<ScoreManager> ().TotalScore);
		int p2Score = Mathf.RoundToInt(GameObject.Find("ScoreManager" + 2).GetComponent<ScoreManager> ().TotalScore);
		if (p1Score > p2Score)
        {
			P1Win.gameObject.SetActive (true);
			P2Win.gameObject.SetActive (false);
			tie.gameObject.SetActive (false);
			WinScore = p1Score;
			Debug.Log ("P1 wins!");
        }
        else if (p2Score > p1Score)
        {
			P1Win.gameObject.SetActive (false);
			P2Win.gameObject.SetActive (true);
			tie.gameObject.SetActive (false);
			WinScore = p2Score;
			Debug.Log ("P2 wins!");
        }
		else if(p1Score == p2Score)
		{
			P1Win.gameObject.SetActive (false);
			P2Win.gameObject.SetActive (false);
			tie.gameObject.SetActive (true);
			WinScore = p1Score;
			Debug.Log ("Tie!");
		}
		score.text = "SCORE: " + WinScore;
		int tempbestresult = 0;
		string BestScore = SceneManager.GetActiveScene ().name;
		if (PlayerPrefs.HasKey (BestScore)) {
			tempbestresult = PlayerPrefs.GetInt (BestScore);
			if (WinScore > tempbestresult) {
				PlayerPrefs.SetInt (BestScore, (int)WinScore);
				nhs.gameObject.SetActive (true);
			} else {
				nhs.gameObject.SetActive (false);
			}
		} else {
			PlayerPrefs.SetInt (BestScore, (int)WinScore);
			//nhs.gameObject.SetActive (false);
		}
    }

}
