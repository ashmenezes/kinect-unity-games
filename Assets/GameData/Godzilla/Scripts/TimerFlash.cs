﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerFlash : MonoBehaviour {

	float Timer1 = 0.5f;
	float Timer2 = 0f;
	bool state = false;
	
	// Update is called once per frame
	void Update () {
		if(state){
			if (Timer1 <= 0) {
				GetComponent<Text> ().color = new Color(178, 0, 0);
				Timer2 = 0.25f;
				state = !state;
			} else {
				Timer1 -= Time.deltaTime;
			}
		}else{
			if (Timer2 <= 0) {
                GetComponent<Text>().color = new Color(255, 255, 255);
				Timer1 = 0.25f;
				state = !state;
			} else {
				Timer2 -= Time.deltaTime;
			}
		}

	}
}
