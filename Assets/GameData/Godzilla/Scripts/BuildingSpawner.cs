﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Spawns buildings
/// </summary>
public class BuildingSpawner : MonoBehaviour {

	public GameObject[] Prefabs;
	public GameObject b;
	float Timer;
	public float buildingDelay = 1;
	public bool isBuilding = false;

	// Use this for initialization
	void Start () {
		if(!Physics.Raycast (transform.position, Vector3.up)){
			Build ();
		}
	}

	/// <summary>
	/// Spawns a random building at current position of the object.
	/// </summary>
	void Build () {
		//Debug.Log ("Calling Build() from " + gameObject.name);
		Vector3 newPos = new Vector3(transform.position.x, transform.position.y -1, transform.position.z);
		b = Instantiate (Prefabs [Random.Range (0, Prefabs.Length)], newPos, Quaternion.identity);
		//GameObject parent = GameObject.Find ("Spawner");
		//b.transform.parent = parent.transform;
		Timer = buildingDelay;
	}

	/// <summary>
	/// Rebuild a buidling after distruction and after sometime
	/// </summary>
	void Update () {
		if (!b && !isBuilding ){//&& !Physics.Raycast (transform.position, Vector3.up)) {
			isBuilding = true;
			if (Timer <= 0) {
				Build ();
			} else {
				Timer -= Time.deltaTime;
			}
			isBuilding = false;
		}
	}
}
