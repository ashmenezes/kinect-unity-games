﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Brakes a mesh into piecices.
/// </summary>
public class TriangleExplosion : MonoBehaviour {

	GameObject Explode;
	GameObject Spawner;
	bool free = false;

	void Start () {
		if (GameObject.Find ("Explosion")) {
			Explode = GameObject.Find ("Explosion");
		}
		//    StartCoroutine(SplitMesh(true));
	}

	void OnCollisionEnter (Collision obj) {
		if (free && obj.collider.CompareTag ("ball") || obj.collider.CompareTag ("Player")) {
			Debug.Log ("Calling OnCollisionEnter in TriangleExplosion from " + gameObject.name);
			free = !free;
			StartCoroutine (SplitMesh (true));
            int Player = int.Parse(obj.collider.name.ToCharArray()[0].ToString());
            GetComponent<ScoreObjectGodzilla>().SendScoreg(Player);
			free = !free;
		}
	}

	public IEnumerator SplitMesh (bool destroy) {
		
		if (GetComponent<MeshFilter> () == null || GetComponent<SkinnedMeshRenderer> () == null) {
			yield return null;
		}

		if (GetComponent<Collider> ()) {
			GetComponent<Collider> ().enabled = false;
		}

		Mesh M = new Mesh ();
		if (GetComponent<MeshFilter> ()) {
			M = GetComponent<MeshFilter> ().mesh;
		} else if (GetComponent<SkinnedMeshRenderer> ()) {
			M = GetComponent<SkinnedMeshRenderer> ().sharedMesh;
		}

		Material[] materials = new Material[0];
		if (GetComponent<MeshRenderer> ()) {
			materials = GetComponent<MeshRenderer> ().materials;
		} else if (GetComponent<SkinnedMeshRenderer> ()) {
			materials = GetComponent<SkinnedMeshRenderer> ().materials;
		}

		if (destroy == true) {
			Destroy (transform.parent.gameObject, 1.2f);
			Destroy (gameObject, 1);
		}

		Vector3[] verts = M.vertices;
		Vector3[] normals = M.normals;
		Vector2[] uvs = M.uv;
		for (int submesh = 0; submesh < M.subMeshCount; submesh++) {

			int[] indices = M.GetTriangles (submesh);
			int Offset = 12;
			for (int i = 0; i < indices.Length; i += Offset) {
				Vector3[] newVerts = new Vector3[Offset];
				Vector3[] newNormals = new Vector3[Offset];
				Vector2[] newUvs = new Vector2[Offset];
				for (int n = 0; n < Offset; n++) {
					//Debug.Log("Check mesh of " + gameObject.name);
					int index = indices [i + n];
					newVerts [n] = verts [index];
					newUvs [n] = uvs [index];
					newNormals [n] = normals [index];
				}

				Mesh mesh = new Mesh ();
				mesh.vertices = newVerts;
				mesh.normals = newNormals;
				mesh.uv = newUvs;

				mesh.triangles = new int[12] {
					0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11//, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29
				};

				GameObject GO = new GameObject ("Triangle " + (i / Offset));
				GO.layer = LayerMask.NameToLayer ("OverlayVideo");
				GO.transform.position = transform.position;
				GO.transform.rotation = transform.rotation;
				GO.AddComponent<MeshRenderer> ().material = materials [submesh];
				GO.AddComponent<MeshFilter> ().mesh = mesh;
				GO.AddComponent<BoxCollider> ();
				Vector3 explosionPos = new Vector3 (transform.position.x + Random.Range (-0.5f, 0.5f), transform.position.y + Random.Range (0f, 0.5f), transform.position.z + Random.Range (-0.5f, 0.5f));
				GO.AddComponent<Rigidbody> ().AddExplosionForce (Random.Range (300, 500), explosionPos, 5);
				Destroy (GO, 5 + Random.Range (0.0f, 5.0f));
			}
		}

		GetComponent<Renderer> ().enabled = false;

		Explode.transform.position = transform.position;
		foreach (var item in Explode.GetComponentsInChildren <ParticleSystem>()) {
			item.Play ();
		}
		Explode.GetComponent <AudioSource> ().Play ();
	}
	/*public IEnumerator SplitMesh (bool destroy)    {

		if(GetComponent<MeshFilter>() == null || GetComponent<SkinnedMeshRenderer>() == null) {
			yield return null;
		}

		if(GetComponent<Collider>()) {
			GetComponent<Collider>().enabled = false;
		}

		Mesh M = new Mesh();
		if(GetComponent<MeshFilter>()) {
			M = GetComponent<MeshFilter>().mesh;
		}
		else if(GetComponent<SkinnedMeshRenderer>()) {
			M = GetComponent<SkinnedMeshRenderer>().sharedMesh;
		}

		Material[] materials = new Material[0];
		if(GetComponent<MeshRenderer>()) {
			materials = GetComponent<MeshRenderer>().materials;
		}
		else if(GetComponent<SkinnedMeshRenderer>()) {
			materials = GetComponent<SkinnedMeshRenderer>().materials;
		}

		Vector3[] verts = M.vertices;
		Vector3[] normals = M.normals;
		Vector2[] uvs = M.uv;
		for (int submesh = 0; submesh < M.subMeshCount; submesh++) {

			int[] indices = M.GetTriangles(submesh);

			for (int i = 0; i < indices.Length; i += 3)    {
				Vector3[] newVerts = new Vector3[3];
				Vector3[] newNormals = new Vector3[3];
				Vector2[] newUvs = new Vector2[3];
				for (int n = 0; n < 3; n++)    {
					int index = indices[i + n];
					newVerts[n] = verts[index];
					newUvs[n] = uvs[index];
					newNormals[n] = normals[index];
				}

				Mesh mesh = new Mesh();
				mesh.vertices = newVerts;
				mesh.normals = newNormals;
				mesh.uv = newUvs;

				mesh.triangles = new int[] { 0, 1, 2, 2, 1, 0 };

				GameObject GO = new GameObject("Triangle " + (i / 3));
				//GO.layer = LayerMask.NameToLayer("Particle");
				GO.transform.position = transform.position;
				GO.transform.rotation = transform.rotation;
				GO.AddComponent<MeshRenderer>().material = materials[submesh];
				GO.AddComponent<MeshFilter>().mesh = mesh;
				GO.AddComponent<BoxCollider>();
				Vector3 explosionPos = new Vector3(transform.position.x + Random.Range(-0.5f, 0.5f), transform.position.y + Random.Range(0f, 0.5f), transform.position.z + Random.Range(-0.5f, 0.5f));
				GO.AddComponent<Rigidbody>().AddExplosionForce(Random.Range(300,500), explosionPos, 5);
				Destroy(GO, 5 + Random.Range(0.0f, 5.0f));
			}
		}

		GetComponent<Renderer>().enabled = false;

		yield return new WaitForSeconds(1.0f);
		if(destroy == true) {
			Destroy(gameObject);
		}

	}*/
}
