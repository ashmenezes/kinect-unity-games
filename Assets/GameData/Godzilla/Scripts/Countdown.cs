﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Countdown : LevelController {

	[Header("Extension")]
	public float Timer = 3;
	public float IntTimer = 3;
	public bool StartCountingDown;
	public UnityEngine.UI.Text CountDownField;

	void Start() {
		IntTimer = Mathf.RoundToInt (Timer);
		Pause ();
	}

	void Update () {
		if (StartCountingDown) {
			//PlayerPrefs.DeleteKey (SceneManager.GetActiveScene().name);
			//GameObject.Find ("GameOver").SetActive(false);
			if (Timer > 0) {
				Timer -= Time.unscaledDeltaTime;
				if (Timer < IntTimer) {
					IntTimer--;
					CountDownField.text = IntTimer.ToString();
					if (IntTimer == 0) {
						CountDownField.text = "GO!";
					}
					if (IntTimer < 0) {
						CountDownField.text = "";
					}
				}
				if (Timer < 0) {
					Timer = 0;
					this.Resume ();
				}
			}
		}
	}

	public void Resume(bool CountDown) {
		StartCountingDown = true;
		Timer = 4;
		IntTimer = 4;
	}

	void OnGUI() {
		if(Input.GetKey(KeyCode.P)) {
			if (GUILayout.Button ("Pause()")) {
				Pause ();
			}
			if (GUILayout.Button ("Resume()")) {
				Resume ();
			}
			if (GUILayout.Button ("Resume(true)")) {
				Resume (true);
			}
		}
	}
}