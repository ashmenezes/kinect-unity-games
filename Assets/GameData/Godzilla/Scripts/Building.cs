﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Rises building for the ground.
/// </summary>
public class Building : MonoBehaviour {

	public float AmountOfShaking = 0.2f;
	public float Decay = 0.007f;
	public AudioClip[] sounds;
	public float limit = 0.01f;

	Quaternion originRotation;
	Vector3 vel;
	float shake_intensity;

	RaycastHit hitInfo;

	// Use this for initialization
	void Start () {
		originRotation = transform.localRotation;
		StartCoroutine (ShakeBuilding ());
	}

	void Update () {
		//Physics.Raycast(
		//if(!hitInfo.collider.name == "Floor"){
		if (!Physics.Raycast (transform.position, Vector3.down)) {
			transform.Translate (Vector3.up * Time.deltaTime * 3.5f);
		} else {
			if (GetComponentInChildren<ParticleSystem> ()) {
				GetComponentInChildren<ParticleSystem> ().Stop ();
				Destroy (GetComponentInChildren<ParticleSystem> (), 5);
			}
		}
	}

	/// <summary>
	/// Finds and destroies the closest building based on Minmim distance. RIGHT NOW THE VALUE IS HARDCODED.
	/// </summary>
	private void FindClosestBuilding () {
		Building[] gos;
		gos = Building.FindObjectsOfType<Building> ();
		Vector3 position = transform.position;
		foreach (Building go in gos) {
			Vector3 diff = go.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			if (go != this && curDistance < 20) {
				Destroy (gameObject);
			}
		}
	}

	#region IEnumerators
	/// <summary>
	/// Shakes building while it is rising.
	/// </summary>
	IEnumerator ShakeBuilding () {
		shake_intensity = AmountOfShaking;
		while (shake_intensity > 0) {
			transform.localRotation = new Quaternion (originRotation.x + UnityEngine.Random.Range (-shake_intensity, shake_intensity) * AmountOfShaking, originRotation.y + UnityEngine.Random.Range (-shake_intensity, shake_intensity) * AmountOfShaking, originRotation.z + UnityEngine.Random.Range (-shake_intensity, shake_intensity) * AmountOfShaking, originRotation.w + UnityEngine.Random.Range (-shake_intensity, shake_intensity) * AmountOfShaking);
			yield return new WaitForSeconds (0.01f);
			shake_intensity -= Decay;
		}
	}

	#endregion
}
