using UnityEngine;
using System.Collections;
using System;

public class SimpleGestureListener : MonoBehaviour, KinectGestures.GestureListenerInterface
{
	// GUI Text to display the gesture messages.
	public GUIText GestureInfo;
	
	// private bool to track if progress message has been displayed
	private bool progressDisplayed;

	LevelController mLevelController;
	PoseRecognition mPoseRecognition;

	public void UserDetected (uint userId, int userIndex)
	{
		// as an example - detect these user specific gestures
		try {
			mLevelController = FindObjectOfType<LevelController> ();
		} catch {
			Debug.Log ("LevelController is not found");
		}
		try {
			mPoseRecognition = FindObjectOfType<PoseRecognition> ();
		} catch {
			Debug.Log ("PoseRecognition is not found");
		}
		KinectManager manager = KinectManager.Instance;

		manager.DetectGesture (userId, KinectGestures.Gestures.Jump);
		manager.DetectGesture (userId, KinectGestures.Gestures.Squat);

//		manager.DetectGesture(userId, KinectGestures.Gestures.Push);
//		manager.DetectGesture(userId, KinectGestures.Gestures.Pull);
		
//		manager.DetectGesture(userId, KinectWrapper.Gestures.SwipeUp);
//		manager.DetectGesture(userId, KinectWrapper.Gestures.SwipeDown);
		
		if (GestureInfo != null) {
			GestureInfo.GetComponent<GUIText> ().text = "SwipeLeft, SwipeRight, Jump or Squat.";
		}
	}

	public void UserLost (uint userId, int userIndex)
	{
		if (GestureInfo != null) {
			GestureInfo.GetComponent<GUIText> ().text = string.Empty;
		}
		if (mLevelController && !mLevelController.IsGameOver) {
			mLevelController.Pause ();
			if (mPoseRecognition) {
				if (mPoseRecognition.Calibrating) {
					mPoseRecognition.StopCalibration ();
				}
			}
		}
	}

	public void GestureInProgress (uint userId, int userIndex, KinectGestures.Gestures gesture, 
	                               float progress, KinectWrapper.NuiSkeletonPositionIndex joint, Vector3 screenPos)
	{
		//GestureInfo.guiText.text = string.Format("{0} Progress: {1:F1}%", gesture, (progress * 100));
		if (gesture == KinectGestures.Gestures.Click && progress > 0.3f) {
			string sGestureText = string.Format ("{0} {1:F1}% complete", gesture, progress * 100);
			if (GestureInfo != null)
				GestureInfo.GetComponent<GUIText> ().text = sGestureText;
			
			progressDisplayed = true;
		} else if ((gesture == KinectGestures.Gestures.ZoomOut || gesture == KinectGestures.Gestures.ZoomIn) && progress > 0.5f) {
			string sGestureText = string.Format ("{0} detected, zoom={1:F1}%", gesture, screenPos.z * 100);
			if (GestureInfo != null)
				GestureInfo.GetComponent<GUIText> ().text = sGestureText;
			
			progressDisplayed = true;
		} else if (gesture == KinectGestures.Gestures.Wheel && progress > 0.5f) {
			string sGestureText = string.Format ("{0} detected, angle={1:F1} deg", gesture, screenPos.z);
			if (GestureInfo != null)
				GestureInfo.GetComponent<GUIText> ().text = sGestureText;
			
			progressDisplayed = true;
		}
	}

	public bool GestureCompleted (uint userId, int userIndex, KinectGestures.Gestures gesture, 
	                              KinectWrapper.NuiSkeletonPositionIndex joint, Vector3 screenPos)
	{
		Debug.Log (gesture + " detected");

		progressDisplayed = false;

		if (gesture == KinectGestures.Gestures.RaiseRightHand) {
			FindObjectOfType<LevelController> ().Restart ();
		}
		if (FindObjectOfType<PlayerContoller> ()) {
			if (gesture == KinectGestures.Gestures.Jump) {
				FindObjectOfType<PlayerContoller> ().Jump ();
			}
		}
		return true;
	}

	public bool GestureCancelled (uint userId, int userIndex, KinectGestures.Gestures gesture, 
	                              KinectWrapper.NuiSkeletonPositionIndex joint)
	{
		if (progressDisplayed) {
			// clear the progress info
			if (GestureInfo != null)
				GestureInfo.GetComponent<GUIText> ().text = String.Empty;
			
			progressDisplayed = false;
		}
		
		return true;
	}
	
}
