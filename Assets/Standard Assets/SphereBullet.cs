﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereBullet : MonoBehaviour {
	public GameObject BrokenBullet;
	//public ParticleSystem fire;
	// Use this for initialization
	void Start () {
		//fire.Play;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter (Collision other){
		Debug.Log ("Collision of DestroyCube");
		if (other.gameObject.name == "katana" )
			DestroyMe ();
		else if (other.gameObject.name == "Body")
			DestroyMe ();
		//fire.Stop;
	}

	public void DestroyMe(){
		////Music.Play ();
		Debug.Log ("Destroy me");
		Destroy (gameObject);

		//ps.Play();
		Instantiate (BrokenBullet, transform.position, transform.rotation);

	}
}
